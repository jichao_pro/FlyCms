package com.flycms.modules.statistics.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.statistics.domain.StatisticsAccessPage;
import com.flycms.modules.statistics.domain.dto.StatisticsAccessPageDTO;
import com.flycms.modules.statistics.service.IStatisticsAccessPageService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 受访页面日报Controller
 * 
 * @author admin
 * @date 2021-01-12
 */
@RestController
@RequestMapping("/system/statistics/page")
public class StatisticsAccessPageAdminController extends BaseController
{
    @Autowired
    private IStatisticsAccessPageService statisticsAccessPageService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////


    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除受访页面日报
     */
    @PreAuthorize("@ss.hasPermi('statistics:page:remove')")
    @Log(title = "受访页面日报", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(statisticsAccessPageService.deleteStatisticsAccessPageByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改受访页面日报
     */
    @PreAuthorize("@ss.hasPermi('statistics:page:edit')")
    @Log(title = "受访页面日报", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StatisticsAccessPage statisticsAccessPage)
    {
        return toAjax(statisticsAccessPageService.updateStatisticsAccessPage(statisticsAccessPage));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询受访页面日报列表
     */
    @PreAuthorize("@ss.hasPermi('statistics:page:list')")
    @GetMapping("/list")
    public TableDataInfo list(StatisticsAccessPage statisticsAccessPage,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<StatisticsAccessPageDTO> pager = statisticsAccessPageService.selectStatisticsAccessPagePager(statisticsAccessPage, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出受访页面日报列表
     */
    @PreAuthorize("@ss.hasPermi('statistics:page:export')")
    @Log(title = "受访页面日报", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StatisticsAccessPage statisticsAccessPage)
    {
        List<StatisticsAccessPageDTO> statisticsAccessPageList = statisticsAccessPageService.exportStatisticsAccessPageList(statisticsAccessPage);
        ExcelUtil<StatisticsAccessPageDTO> util = new ExcelUtil<StatisticsAccessPageDTO>(StatisticsAccessPageDTO.class);
        return util.exportExcel(statisticsAccessPageList, "page");
    }

    /**
     * 获取受访页面日报详细信息
     */
    @PreAuthorize("@ss.hasPermi('statistics:page:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(statisticsAccessPageService.findStatisticsAccessPageById(id));
    }

}
