package com.flycms.modules.fullname.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 姓氏对象 fly_surname
 * 
 * @author admin
 * @date 2020-10-13
 */
@Data
public class Surname extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 姓氏 */
    private String lastName;
}
