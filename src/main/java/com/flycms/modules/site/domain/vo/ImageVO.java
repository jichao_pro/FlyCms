package com.flycms.modules.site.domain.vo;

import lombok.Data;

@Data
public class ImageVO {
    /** 产品编码 */
    private String picName;
    /** 单位 */
    private String picUrl;
}
