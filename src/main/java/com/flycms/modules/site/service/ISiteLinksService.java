package com.flycms.modules.site.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.site.domain.SiteLinks;
import com.flycms.modules.site.domain.dto.SiteLinksDTO;

/**
 * 友情链接Service接口
 * 
 * @author admin
 * @date 2020-07-08
 */
public interface ISiteLinksService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增友情链接
     *
     * @param siteLinks 友情链接
     * @return 结果
     */
    public int insertSiteLinks(SiteLinks siteLinks);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除友情链接
     *
     * @param ids 需要删除的友情链接ID
     * @return 结果
     */
    public int deleteSiteLinksByIds(Long[] ids);

    /**
     * 删除友情链接信息
     *
     * @param id 友情链接ID
     * @return 结果
     */
    public int deleteSiteLinksById(Long id);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改友情链接
     *
     * @param siteLinks 友情链接
     * @return 结果
     */
    public int updateSiteLinks(SiteLinks siteLinks);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验网站名称是否唯一
     *
     * @param siteLinks 友情链接
     * @return 结果
     */
    public String checkSiteLinksLinkNameUnique(SiteLinks siteLinks);


    /**
     * 查询友情链接
     * 
     * @param id 友情链接ID
     * @return 友情链接
     */
    public SiteLinks selectSiteLinksById(Long id);

    /**
     * 查询友情链接列表
     * 
     * @param siteLinks 友情链接
     * @return 友情链接集合
     */
    public Pager<SiteLinksDTO> selectSiteLinksPager(SiteLinks siteLinks, Integer page, Integer limit, String sort, String order);
}
