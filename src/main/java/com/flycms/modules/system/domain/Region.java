package com.flycms.modules.system.domain;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.web.domain.TreeEntity;
import lombok.Data;

/**
 * 行政区域对象 fly_region
 * 
 * @author admin
 * @date 2020-05-31
 */
@Data
public class Region extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 父分类ID */
    private Long parentId;
    /** 行政区域名称 */
    private String regionName;
    /** 行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县 */
    private Integer regionType;
    /** 行政区域编码 */
    private Long regionCode;
}
