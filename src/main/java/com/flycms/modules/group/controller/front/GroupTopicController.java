package com.flycms.modules.group.controller.front;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.elastic.service.IElasticSearchService;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.GroupTopicComment;
import com.flycms.modules.group.domain.GroupUser;
import com.flycms.modules.group.domain.dto.GroupTopicCommentDTO;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import com.flycms.modules.group.domain.vo.GroupTopicVO;
import com.flycms.modules.group.service.IGroupService;
import com.flycms.modules.group.service.IGroupTopicCommentService;
import com.flycms.modules.group.service.IGroupTopicService;
import com.flycms.modules.group.service.IGroupUserService;
import com.flycms.modules.score.service.IScoreRuleService;
import com.flycms.modules.site.domain.Site;
import com.flycms.modules.site.service.ISiteService;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.service.IUserAccountService;
import com.flycms.modules.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 小组话题Controller
 * 
 * @author admin
 * @date 2020-07-08
 */
@Controller
public class GroupTopicController extends BaseController
{
    @Autowired
    private IGroupTopicService groupTopicService;

    @Autowired
    private IGroupService groupService;

    @Autowired
    private IGroupUserService groupUserService;

    @Autowired
    private IGroupTopicCommentService groupTopicCommentService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IUserAccountService userAccountService;

    @Autowired
    private ISiteService siteService;

    @Autowired
    private IScoreRuleService scoreRuleService;

    @Autowired
    private IElasticSearchService elasticSearchService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 添加话题
     *
     * @return
     */
    @GetMapping("/user/group/{groupId}/topic/create")
    public String create(@PathVariable("groupId") String groupId,ModelMap modelMap){
        if (!StrUtils.checkLong(groupId)) {
            return forward("/error/404");
        }
        Group group= groupService.findGroupById(Long.valueOf(groupId));
        if(group == null){
            return forward("/error/404");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            modelMap.addAttribute("message","用户未登录");
            return theme.getPcTemplate("/error/error_tip");
        }
        GroupUser findgroupUser = new GroupUser();
        findgroupUser.setGroupId(group.getId());
        findgroupUser.setUserId(user.getId());
        GroupUser userDTO=groupUserService.findGroupUser(findgroupUser);
        if(userDTO == null || userDTO.getStatus() == 0){
            modelMap.addAttribute("message","用户未加入或者加入申请未审核");
            return theme.getPcTemplate("/error/error_tip");
        }
        modelMap.addAttribute("group",group);
        modelMap.addAttribute("groupId",groupId);
        return theme.getPcTemplate("topic/create");
    }

    /**
     * 新增群组(小组)话题
     */
    @PostMapping("/user/topic/create")
    @ResponseBody
    public AjaxResult add(GroupTopicVO topicVo) throws Exception
    {
        if(StrUtils.isEmpty(topicVo.getGroupId())){
            return AjaxResult.error("小组ID不得为空！");
        }
        if (!StrUtils.checkLong(topicVo.getGroupId())) {
            return AjaxResult.error("未选择加入发布的小组");
        }
        if(StrUtils.isEmpty(topicVo.getTitle())){
            return AjaxResult.error("标题不得为空！");
        }
        if(StrUtils.isEmpty(topicVo.getContent())){
            return AjaxResult.error("内容不得为空！");
        }
        Group group= groupService.findGroupById(Long.valueOf(topicVo.getGroupId()));
        if(group == null){
            return AjaxResult.error("小组不存在");
        }
        if("0".equals(group.getIspost())){
            return AjaxResult.error("本小组暂不允许发帖！");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            return AjaxResult.error("未登录或者用户不存在");
        }
        GroupUser findgroupUser = new GroupUser();
        findgroupUser.setGroupId(group.getId());
        findgroupUser.setUserId(user.getId());
        GroupUser userDTO=groupUserService.findGroupUser(findgroupUser);
        if(userDTO == null || userDTO.getStatus() == 0){
            return AjaxResult.error("您未关注本小组或者未通过审核");
        }
        GroupTopic groupTopic=new GroupTopic();
        groupTopic.setGroupId(group.getId());
        groupTopic.setTitle(topicVo.getTitle());
        groupTopic.setContent(topicVo.getContent());
        groupTopic.setColumnId(Long.valueOf(topicVo.getColumnId()));
        groupTopic.setIscomment(topicVo.getIscomment());
        groupTopic.setIscommentshow(topicVo.getIscommentshow());
        groupTopic.setScore(topicVo.getScore());
        //是否发帖审核  0不允许，1允许
        if("1".equals(group.getIspostaudit())){
            groupTopic.setIsaudit("0");
        }else{
            groupTopic.setIsaudit("1");
        }
        Site site=siteService.selectSite();
        if(site != null){
            //是否发帖审核  0不允许，1允许
            if(site.getPostVerify() ==1){
                groupTopic.setStatus("0");
            }else{
                groupTopic.setStatus("1");
            }
        }
        if (UserConstants.NOT_UNIQUE.equals(groupTopicService.checkGroupTopicNameUnique(groupTopic)))
        {
            return AjaxResult.error("发布标题'" + groupTopic.getTitle() + "'失败，请勿重复发布！");
        }
        int conut=groupTopicService.insertGroupTopic(groupTopic);
        if(conut>0){
            if("1".equals(group.getIspostaudit())){
                return AjaxResult.success("本栏内容需要审核，请耐心等待！",groupTopic.getId().toString());
            }
            userAccountService.updateUserTopic(user.getId());
            groupService.updateCountTopic(groupTopic.getGroupId());

            Map<String, Object> map = new HashMap<>();
            map.put("title", groupTopic.getTitle());
            map.put("content", groupTopic.getContent());
            elasticSearchService.createDocument("topic",groupTopic.getId().toString(),map);
            return AjaxResult.success("发布成功",groupTopic.getId().toString());
        }
        return AjaxResult.error("未知错误！");
    }

    /**
     * 新增群组(小组)话题评论
     */
    @PostMapping("/user/add/topic/comment")
    @ResponseBody
    public AjaxResult addComment(@RequestParam(value = "topicId", required = false) String topicId,
                          @RequestParam(value = "content", required = false) String content,
                          @RequestParam(value = "ispublic", required = false) String ispublic){
        if(StrUtils.isEmpty(topicId)){
            return AjaxResult.error("话题id不得为空！");
        }
        if (!StrUtils.checkLong(topicId)) {
            return AjaxResult.error("话题id错误");
        }

        if(StrUtils.isEmpty(content)){
            return AjaxResult.error("内容不得为空！");
        }

        GroupTopicDTO topic= groupTopicService.findGroupTopicById(Long.valueOf(topicId));
        if(topic == null){
            return AjaxResult.error("话题不存在");
        }
        if("1".equals(topic.getIscomment())){
            return AjaxResult.error("该话题不允许评论");
        }
        GroupTopicComment topicComment = new GroupTopicComment();
        topicComment.setTopicId(Long.valueOf(topicId));
        topicComment.setContent(content);
        topicComment.setIspublic(Integer.valueOf(ispublic));
        topicComment.setUserId(SessionUtils.getUser().getId());
        if (UserConstants.NOT_UNIQUE.equals(groupTopicCommentService.checkGroupTopicCommentUnique(topicComment)))
        {
            return AjaxResult.error("该评论您已发布！");
        }
        Site site=siteService.selectSite();
        if(site != null){
            //是否发帖审核  0不允许，1允许
            if(site.getCommentVerify() ==1){
                topicComment.setStatus(0);
            }else{
                topicComment.setStatus(1);
            }
        }
        int conut=groupTopicCommentService.insertGroupTopicComment(topicComment);
        if(conut > 0){
            if(topicComment.getStatus() == 1){
                AjaxResult.success("评论已提交，需要审核后才能显示！",topicComment.getTopicId().toString());
            }
            groupTopicService.updateCountComment(Long.valueOf(topicId));
            return AjaxResult.success("发布成功",topicComment.getTopicId().toString());
        }
        return AjaxResult.error("未知错误！");
    }

    /**
     * 新增回复评论
     */
    @PostMapping("/user/add/topic/reply")
    @ResponseBody
    public AjaxResult addReply(@RequestParam(value = "commentId", required = false) String commentId,
                          @RequestParam(value = "content", required = false) String content){
        if(StrUtils.isEmpty(commentId)){
            return AjaxResult.error("回复内容id不得为空！");
        }
        if (!StrUtils.checkLong(commentId)) {
            return AjaxResult.error("内容id错误");
        }
        if(StrUtils.isEmpty(content)){
            return AjaxResult.error("内容不得为空！");
        }
        GroupTopicCommentDTO comment=groupTopicCommentService.findGroupTopicCommentById(Long.valueOf(commentId));
        if(comment == null){
            return AjaxResult.error("回复的内容不存在");
        }
        GroupTopicComment topicComment = new GroupTopicComment();
        topicComment.setTopicId(comment.getTopicId());
        topicComment.setReferId(Long.valueOf(commentId));
        topicComment.setContent(content);
        topicComment.setUserId(SessionUtils.getUser().getId());
        if (UserConstants.NOT_UNIQUE.equals(groupTopicCommentService.checkGroupTopicCommentUnique(topicComment)))
        {
            return AjaxResult.error("该评论您已发布！");
        }
        Site site=siteService.selectSite();
        if(site != null){
            //是否发帖审核  0不允许，1允许
            if(site.getCommentVerify() ==1){
                topicComment.setStatus(0);
            }else{
                topicComment.setStatus(1);
            }
        }
        int conut=groupTopicCommentService.insertGroupTopicComment(topicComment);
        if(conut > 0){
            if(topicComment.getStatus() == 1){
                AjaxResult.success("评论已提交，需要审核后才能显示！",topicComment.getTopicId().toString());
            }
            groupTopicService.updateCountComment(comment.getTopicId());
            return AjaxResult.success("发布成功",topicComment.getTopicId().toString());
        }
        return AjaxResult.error("未知错误！");
    }
    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户账户
     */
    @PostMapping("/user/topic/delete")
    @ResponseBody
    public AjaxResult remove(@RequestParam("id") String id)
    {
        if (!StrUtils.checkLong(id)) {
            return AjaxResult.error("话题id错误");
        }
        GroupTopicDTO topic= groupTopicService.findGroupTopicById(Long.valueOf(id));
        if(topic == null){
            return AjaxResult.error("话题不存在");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            return AjaxResult.error("未登录或者用户不存在");
        }
        if(!user.getId().equals(topic.getUserId())){
            return AjaxResult.error("不能删除不属于您的文章！");
        }
        int count = groupTopicService.updateDeleteGroupTopicById(Long.valueOf(id));
        if(count == 0){
            return AjaxResult.error("删除话题失败！");
        }
        groupService.updateCountTopic(topic.getGroupId());
        groupTopicService.updateUserCountTopic(topic.getUserId());
        groupTopicService.updateCountComment(topic.getId());
        // 删帖扣分
        scoreRuleService.scoreRuleBonus(topic.getId(), 547413101788803091L,topic.getId());
        return AjaxResult.success("话题删除成功","/group/"+topic.getGroupId());
    }

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 添加话题
     *
     * @return
     */
    @GetMapping("/user/my/topic/{topicId}/update/")
    public String updateGroupTopic(@PathVariable("topicId") String topicId,ModelMap modelMap){
        if (!StrUtils.checkLong(topicId)) {
            return forward("/error/404");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            modelMap.addAttribute("message","用户未登录");
            return theme.getPcTemplate("/error/error_tip");
        }

        GroupTopicDTO topic= groupTopicService.findGroupTopicById(Long.valueOf(topicId));
        if(topic == null){
            return forward("/error/404");
        }
        if(!user.getId().equals(topic.getUserId())){
            modelMap.addAttribute("message","不能修改不属于您的文章！");
            return theme.getPcTemplate("/error/error_tip");
        }
        modelMap.addAttribute("topic",topic);
        return theme.getPcTemplate("topic/create");
    }


    /**
     * 修改小组话题
     */
    /**
     * 新增群组(小组)
     */
    @PostMapping("/user/my/topic/update")
    @ResponseBody
    public AjaxResult updateGroupTopic(GroupTopicVO topicVo) throws Exception
    {
        if (!StrUtils.checkLong(topicVo.getId())) {
            return AjaxResult.error("话题id错误");
        }
        GroupTopicDTO topic= groupTopicService.findGroupTopicById(Long.valueOf(topicVo.getId()));
        if(topic == null){
            return AjaxResult.error("话题不存在");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            return AjaxResult.error("未登录或者用户不存在");
        }
        if(!user.getId().equals(topic.getUserId())){
            return AjaxResult.error("不能修改不属于您的文章！");
        }

        Group group= groupService.findGroupById(topic.getGroupId());
        if(group == null){
            return AjaxResult.error("小组不存在");
        }
        GroupTopic groupTopic=new GroupTopic();
        groupTopic.setId(Long.valueOf(topicVo.getId()));
        groupTopic.setTitle(topicVo.getTitle());
        groupTopic.setContent(topicVo.getContent());
        groupTopic.setUserId(user.getId());
        groupTopic.setColumnId(Long.valueOf(topicVo.getColumnId()));
        groupTopic.setIscomment(topicVo.getIscomment());
        groupTopic.setIscommentshow(topicVo.getIscommentshow());
        groupTopic.setScore(topicVo.getScore());
        //是否发帖审核  0不允许，1允许
        if("1".equals(group.getIspostaudit())){
            groupTopic.setIsaudit("0");
        }else{
            groupTopic.setIsaudit("1");
        }
        Site site=siteService.selectSite();
        if(site != null){
            //是否发帖审核  0不允许，1允许
            if(site.getPostVerify() ==1){
                groupTopic.setStatus("0");
            }else{
                groupTopic.setStatus("1");
            }
        }
        if (UserConstants.NOT_UNIQUE.equals(groupTopicService.checkGroupTopicNameUnique(groupTopic)))
        {
            return AjaxResult.error("修改标题'" + groupTopic.getTitle() + "'失败，请勿重复发布！");
        }
        int count = groupTopicService.updateGroupTopic(groupTopic);
        if(count >0){
            if("1".equals(group.getIspostaudit())){
                return AjaxResult.success("本栏内容需要审核，请耐心等待！",groupTopic.getId().toString());
            }
            userAccountService.updateUserTopic(user.getId());
            groupService.updateCountTopic(groupTopic.getGroupId());
            return AjaxResult.success("修改成功",groupTopic.getId().toString());
        }
        return AjaxResult.error("未知错误！");
    }

    /**
     * 更新话题浏览数量
     */
    @GetMapping("/topic/update/view")
    @ResponseBody
    public AjaxResult updateCountView(@RequestParam("id") String id)
    {
        if (StrUtils.checkLong(id)) {
            groupTopicService.updateCountView(Long.valueOf(id));
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 发现首页
     *
     * @return
     */
    @GetMapping(value = {"/topic/" , "/topic/index","/topic/p/{p}" , "/topic/index/p/{p}"})
    public String topicList(@PathVariable(value = "p", required = false) String p,ModelMap modelMap){
        if(p == null) p = "1";
        modelMap.addAttribute("p", p);
        return theme.getPcTemplate("topic/list_topic");
    }

    /**
     * 热门内容首页
     *
     * @return
     */
    @GetMapping(value = {"/topic-hot/" , "/topic-hot/index","/topic-hot/p/{p}" , "/topic-hot/index/p/{p}"})
    public String topicHotList(@PathVariable(value = "p", required = false) String p,ModelMap modelMap){
        if(p == null) p = "1";
        modelMap.addAttribute("p", p);
        return theme.getPcTemplate("topic/list_hot");
    }

    /**
     * 推荐内容首页
     *
     * @return
     */
    @GetMapping(value = {"/recommend/" , "/recommend/index","/recommend/p/{p}" , "/recommend/index/p/{p}"})
    public String topicRecommendList(@PathVariable(value = "p", required = false) String p,ModelMap modelMap){
        if(p == null) p = "1";
        modelMap.addAttribute("p", p);
        return theme.getPcTemplate("topic/list_recommend");
    }

    /**
     * 详细内容
     *
     * @return
     */
    @GetMapping(value = {"/topic/{id}/" , "/topic/{id}","/topic/{id}/p/{p}"})
    public String detail(@PathVariable("id") String id,@PathVariable(value = "p", required = false) String p, ModelMap modelMap){
        if (!StrUtils.checkLong(id)) {
            return forward("/error/404");
        }
        GroupTopicDTO content=groupTopicService.findGroupTopicById(Long.valueOf(id));
        if(content == null){
            return forward("/error/404");
        }
        if(!"1".equals(content.getIsaudit())){
            modelMap.addAttribute("message","该内容小组管理员未审核");
            return theme.getPcTemplate("/error/error_tip");
        }
        if(!"1".equals(content.getStatus())){
            modelMap.addAttribute("message","该内容管理员未审核或者未审核通过，请联系管理员");
            return theme.getPcTemplate("/error/error_tip");
        }
        if(p == null) p = "1";
        modelMap.addAttribute("p", p);
        modelMap.addAttribute("content",content);
        return theme.getPcTemplate("topic/detail");
    }


}
