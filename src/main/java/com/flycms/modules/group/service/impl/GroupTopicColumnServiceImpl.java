package com.flycms.modules.group.service.impl;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.modules.group.domain.GroupTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.mapper.GroupTopicColumnMapper;
import com.flycms.modules.group.domain.GroupTopicColumn;
import com.flycms.modules.group.domain.dto.GroupTopicColumnDTO;
import com.flycms.modules.group.service.IGroupTopicColumnService;

import java.util.ArrayList;
import java.util.List;

/**
 * 帖子分类Service业务层处理
 * 
 * @author admin
 * @date 2020-11-03
 */
@Service
public class GroupTopicColumnServiceImpl implements IGroupTopicColumnService 
{
    @Autowired
    private GroupTopicColumnMapper groupTopicColumnMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增帖子分类
     *
     * @param groupTopicColumn 帖子分类
     * @return 结果
     */
    @Override
    public int insertGroupTopicColumn(GroupTopicColumn groupTopicColumn)
    {
        groupTopicColumn.setId(SnowFlakeUtils.nextId());
        groupTopicColumn.setCreateTime(DateUtils.getNowDate());
        return groupTopicColumnMapper.insertGroupTopicColumn(groupTopicColumn);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除帖子分类
     *
     * @param ids 需要删除的帖子分类ID
     * @return 结果
     */
    @Override
    public int deleteGroupTopicColumnByIds(Long[] ids)
    {
        return groupTopicColumnMapper.deleteGroupTopicColumnByIds(ids);
    }

    /**
     * 删除帖子分类信息
     *
     * @param id 帖子分类ID
     * @return 结果
     */
    @Override
    public int deleteGroupTopicColumnById(Long id)
    {
        return groupTopicColumnMapper.deleteGroupTopicColumnById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改帖子分类
     *
     * @param groupTopicColumn 帖子分类
     * @return 结果
     */
    @Override
    public int updateGroupTopicColumn(GroupTopicColumn groupTopicColumn)
    {
        groupTopicColumn.setUpdateTime(DateUtils.getNowDate());
        return groupTopicColumnMapper.updateGroupTopicColumn(groupTopicColumn);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询帖子分类数量
     *
     * @param groupTopicColumn 帖子分类
     * @return 帖子分类数量
     */
    @Override
    public String checkGroupTopicColumnUnique(GroupTopicColumn groupTopicColumn)
    {
        int count = groupTopicColumnMapper.checkGroupTopicColumnUnique(groupTopicColumn);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 查询帖子分类
     * 
     * @param id 帖子分类ID
     * @return 帖子分类
     */
    @Override
    public GroupTopicColumnDTO findGroupTopicColumnById(Long id)
    {
        GroupTopicColumn  groupTopicColumn=groupTopicColumnMapper.findGroupTopicColumnById(id);
        return BeanConvertor.convertBean(groupTopicColumn,GroupTopicColumnDTO.class);
    }


    /**
     * 查询帖子分类列表
     *
     * @param groupTopicColumn 帖子分类
     * @return 帖子分类
     */
    @Override
    public Pager<GroupTopicColumnDTO> selectGroupTopicColumnPager(GroupTopicColumn groupTopicColumn, Integer page, Integer limit, String sort, String order)
    {
        Pager<GroupTopicColumnDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }else{
            pager.addOrderProperty("id", true,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupTopicColumn);

        List<GroupTopicColumn> groupTopicColumnList=groupTopicColumnMapper.selectGroupTopicColumnPager(pager);
        List<GroupTopicColumnDTO> dtolsit = new ArrayList<GroupTopicColumnDTO>();
        groupTopicColumnList.forEach(entity -> {
            GroupTopicColumnDTO dto = new GroupTopicColumnDTO();
            dto.setId(entity.getId());
            dto.setGroupId(entity.getGroupId());
            dto.setUserId(entity.getUserId());
            dto.setColumnName(entity.getColumnName());
            dto.setSortOrder(entity.getSortOrder());
            dto.setCountTopic(entity.getCountTopic());
            dto.setStatus(entity.getStatus());
            dto.setCreateTime(entity.getCreateTime());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupTopicColumnMapper.queryGroupTopicColumnTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的帖子分类列表
     *
     * @param groupTopicColumn 帖子分类
     * @return 帖子分类集合
     */
    @Override
    public List<GroupTopicColumnDTO> selectGroupTopicColumnList(GroupTopicColumn groupTopicColumn) {
        return BeanConvertor.copyList(groupTopicColumnMapper.selectGroupTopicColumnList(groupTopicColumn),GroupTopicColumnDTO.class);
    }
}
