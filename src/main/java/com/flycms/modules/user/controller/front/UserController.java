package com.flycms.modules.user.controller.front;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.*;
import com.flycms.common.utils.ip.IpUtils;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.notify.service.IEmailService;
import com.flycms.modules.site.domain.Site;
import com.flycms.modules.site.service.ISiteService;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.domain.UserActivation;
import com.flycms.modules.user.domain.dto.UserInfoDTO;
import com.flycms.modules.user.service.IUserActivationService;
import com.flycms.modules.user.service.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户后台Controller
 * 
 * @author admin
 * @date 2020-07-08
 */
@Controller
public class UserController extends BaseController
{
    private static final Logger log = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private ISiteService siteService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IEmailService emailService;

    @Autowired
    private IUserActivationService userActivationService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 邮箱注册
     *
     * @return
     */
    @GetMapping(value = "/register/email")
    public String regEmail(ModelMap modelMap){
        return theme.getPcTemplate("/user/register");
    }

    /**
     * 邮箱获取验证码
     *
     * @param username
     * @param captcha  备用图形验证
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/register/send/email")
    public AjaxResult regEmailCode(@RequestParam(value = "username", required = false) String username,
                                   @RequestParam(value = "captcha", required = false) String captcha){
        if(!StrUtils.isEmail(username)){
            return AjaxResult.error("邮箱格式错误");
        }
        User entity = userService.findUserByEmail(username);
        if(entity!=null){
            return AjaxResult.error("该邮箱已被占用！");
        }
        String newCode=StrUtils.getUserCaptcha().toLowerCase();
        emailService.sendEmail(username,newCode,"reg_email");
        UserActivation userActivation=new UserActivation();
        userActivation.setUserName(username);
        userActivation.setCode(newCode);
        userActivation.setCodeType(1);
        userActivation.setReferStatus(0);
        userActivationService.insertUserActivation(userActivation);
        return AjaxResult.success("验证码已发送到您邮箱");
    }

    /**
     * 增加邮箱注册用户
     */
    @ResponseBody
    @PostMapping(value = "/register/add/email")
    public AjaxResult addEmailUser(@RequestParam(value = "username", required = false) String username,
                                   @RequestParam(value = "code", required = false) String code,
                                   @RequestParam(value = "password", required = false) String password,
                                    @RequestParam(value = "password2", required = false) String password2,
                                    @RequestParam(value = "invite", required = false) String invite,
                                    @RequestParam(value = "captcha", required = false) String captcha)
    {
        username=username.trim();
        if(StrUtils.isEmpty(username)){
            return AjaxResult.error("用户名不能为空");
        }
        if(!StrUtils.isEmail(username)){
            return AjaxResult.error("邮箱格式错误");
        }
        password=password.trim();
        if(StrUtils.isEmpty(password)){
            return AjaxResult.error("密码不能为空");
        }
        password2=password2.trim();
        if(!password.equals(password2)){
            return AjaxResult.error("两次密码不一样");
        }

        if(!StrUtils.isEmpty(captcha)){
            captcha=captcha.trim();
            String kaptcha = (String) session.getAttribute("captcha");
            if(!captcha.equals(kaptcha)){
                return AjaxResult.error("验证码错误");
            }
        }
        User entity = userService.findUserByEmail(username);
        if(entity!=null){
            return AjaxResult.error("该邮箱已被占用！");
        }
        if(userActivationService.checkUserActivationCode(username,1,code)){
            return AjaxResult.error("验证码错误或者已失效！");
        }
        //修改验证码状态
        userActivationService.updateUserActivationByStatus(username,code);
        User user= new User();
        user.setEmail(username);
        //给用户默认随机一个昵称
        user.setNickname(NickNameUtils.generateName());
        String newPassword=SecurityUtils.encryptPassword(password);
        user.setPassword(newPassword);
        user.setLastLoginIp(IpUtils.getIpAddr(request));
        Site site=siteService.selectSite();
        if(site.getUserVerify() == 0){
            user.setStatus(1);
        }else{
            user.setStatus(0);
        }
        int conut=userService.insertUser(user);
        if(conut>0){
            SessionUtils.setUser(user);
            return AjaxResult.success("注册成功","/user/my/setting");
        }
        return AjaxResult.error("注册失败，请联系管理员");
    }

    /**
     * 电话注册
     *
     * @return
     */
    @GetMapping(value = "/register/phone")
    public String regPhone(ModelMap modelMap){
        return theme.getPcTemplate("/user/register_phone");
    }

    /**
     * 新增手机号码注册用户
     */
    @PreAuthorize("@ss.hasPermi('user:user:add')")
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @PostMapping(value = "/register/add/phone")
    public AjaxResult add(@RequestParam(value = "username", required = false) String username,
                          @RequestParam(value = "password", required = false) String password,
                          @RequestParam(value = "password2", required = false) String password2,
                          @RequestParam(value = "invite", required = false) String invite,
                          @RequestParam(value = "captcha", required = false) String captcha)
    {
        username=username.trim();
        if(StrUtils.isEmpty(username)){
            return AjaxResult.error("用户名不能为空");
        }
        password=password.trim();
        if(StrUtils.isEmpty(password)){
            return AjaxResult.error("密码不能为空");
        }
        password2=password2.trim();
        if(!password.equals(password2)){
            return AjaxResult.error("两次密码不一样");
        }
        captcha=captcha.trim();
        if(!StrUtils.isEmpty(captcha)){
            String kaptcha = (String) session.getAttribute("captcha");
            if(!captcha.equals(kaptcha)){
                return AjaxResult.error("验证码错误");
            }
        }
        User user= new User();
        user.setMobile(username);
        String newPassword=SecurityUtils.encryptPassword(password);
        user.setPassword(newPassword);
        //给用户默认随机一个昵称
        user.setNickname(NickNameUtils.generateName());
        Site site=siteService.selectSite();
        if(site.getUserVerify() == 0){
            user.setStatus(1);
        }else{
            user.setStatus(0);
        }
        int conut=userService.insertUser(user);
        if(conut>0){
            SessionUtils.setUser(user);
            return AjaxResult.success("注册成功","/user/my/");
        }
        return AjaxResult.error("注册失败，请联系管理员");
    }

    /**
     * 找回密码，邮箱获取验证码
     *
     * @param username
     * @param captcha  备用图形验证
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/password/reset/email")
    public AjaxResult backEmailCode(@RequestParam(value = "username", required = false) String username,
                                   @RequestParam(value = "captcha", required = false) String captcha){
        if(!StrUtils.isEmail(username)){
            return AjaxResult.error("邮箱格式错误");
        }
        User entity = userService.findUserByEmail(username);
        if(entity!=null){
            return AjaxResult.error("该邮箱已被占用！");
        }
        String newCode=StrUtils.getUserCaptcha().toLowerCase();
        emailService.sendEmail(username,newCode,"reset_email");
        UserActivation userActivation=new UserActivation();
        userActivation.setUserName(username);
        userActivation.setCode(newCode);
        userActivation.setCodeType(3);
        userActivation.setReferStatus(0);
        userActivationService.insertUserActivation(userActivation);
        return AjaxResult.success("验证码已发送到您邮箱");
    }
    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////

    /**
     * 保存头像
     *
     * @param avatar
     * @return
     * @throws IOException
     * @throws ParseException
     */
    @ResponseBody
    @PostMapping("/user/update/avatar")
    public AjaxResult changeAvatar(String avatar) throws IOException, ParseException {
        if (StringUtils.isEmpty(avatar)) {
            return AjaxResult.error("头像不能为空");
        }
        User user = SessionUtils.getUser();
        if(user == null){
            return AjaxResult.error("请登录后再上传头像");
        }
        byte[] bytes;
        try {
            String _avatar = avatar.substring(avatar.indexOf(",") + 1, avatar.length());
            bytes = Base64HelperUtils.decode(_avatar);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("头像格式不正确");
        }
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
        BufferedImage bufferedImage = ImageIO.read(bais);
        userService.updateUserAvatar(user, bufferedImage);
        bais.close();
        return AjaxResult.success();
    }

    @ResponseBody
    @PostMapping(value = "/user/update/password")
    public AjaxResult updatePassword(
            @RequestParam(value = "old_password", required = false) String old_password,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "password_confirmation", required = false) String password_confirmation,
            @RequestParam(value = "captcha", required = false) String captcha) {
        String kaptcha = (String) session.getAttribute("kaptcha");
        // 校验验证码
        if (captcha == null && "".equals(captcha)) {
            return AjaxResult.error("验证码不能为空");
        }
        captcha=captcha.toLowerCase();
        if(!captcha.equals(kaptcha)){
            return AjaxResult.error("验证码错误");
        }
        if (StringUtils.isBlank(old_password)) {
            return AjaxResult.error("原来密码不能为空");
        } else if (old_password.length() < 6 && old_password.length() >= 32) {
            return AjaxResult.error("密码最少6个字符，最多32个字符");
        }
        if (StringUtils.isBlank(password)) {
            return AjaxResult.error("新密码不能为空");
        } else if (password.length() < 6 && password.length() >= 32) {
            return AjaxResult.error("密码最少6个字符，最多32个字符");
        }
        if (!password.equals(password_confirmation)) {
            return AjaxResult.error("两次密码必须一样");
        }
        userService.updatePassword(SessionUtils.getUser().getId(), password);
        SessionUtils.removeUser();
        return AjaxResult.success("密码修改成功，请重新登录","/user/login/");
    }

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 用户登录
     *
     * @return
     */
    @GetMapping(value = {"/user/login/" , "/user/login"})
    public String userLogin(@RequestParam(value = "redirectUrl",required = false) String redirectUrl,ModelMap modelMap){
        User user = SessionUtils.getUser();
        if(user != null){
            return redirect("/");
        }
        modelMap.addAttribute("redirectUrl",redirectUrl);
        return theme.getPcTemplate("user/login");
    }

    //登录处理
    @ResponseBody
    @PostMapping(value = "/user/login")
    public AjaxResult userLogin(
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "password", required = false) String password,
            @RequestParam(value = "rememberMe", required = false) String rememberMe,
            @RequestParam(value = "redirectUrl",required = false) String redirectUrl) {
        try {
            if (StringUtils.isBlank(username)) {
                return AjaxResult.error("用户名不能为空");
            }
            if (StringUtils.isBlank(password)) {
                return AjaxResult.error("密码不能为空");
            } else if (password.length() < 6 && password.length() > 30) {
                return AjaxResult.error("密码最少6个字符，最多30个字符");
            }

            boolean keepLogin = "1".equals(rememberMe) ? true : false;
            User entity = null;
            if(StrUtils.checkUserName(username)){
                entity = userService.findUserByUsername(username);
            }else if(StrUtils.checkPhoneNumber(username)){
                entity = userService.findUserByMobile(username);
            }else if(StrUtils.checkEmail(username)){
                entity = userService.findUserByEmail(username);
            }
            if(entity==null){
                return AjaxResult.error("帐号不存在");
            }else{
                if(!BCrypt.checkpw(password.trim().toString(), entity.getPassword())){
                    return AjaxResult.error("密码错误");
                }
                User loginuser=new User();
                loginuser.setLastLoginTime(DateUtils.getNowDate());
                loginuser.setLastLoginIp(IpUtils.getIpAddr(request));
                userService.updateUser(loginuser);
                SessionUtils.setUser(entity);
                if (!StrUtils.isEmpty(redirectUrl)){
                    return AjaxResult.success("操作成功", redirectUrl);
                }
                return AjaxResult.success("操作成功", "/people/");
            }
        } catch (Exception e) {
            return AjaxResult.error("帐号或密码错误。");
        }
    }

    /*
     *
     * 前台JS读取用户登录状态判断
     *
     */
    @ResponseBody
    @PostMapping(value = "/user/status")
    public AjaxResult userStatus() throws Exception {
        Map<String, Object> map = new HashMap<>();
        User user = SessionUtils.getUser();
        if(user!=null){
            map.put("userLogin", 1);
        }else{
            map.put("userLogin", 0);
        }
        return AjaxResult.success("登录状态",map);
    }


    /**
     * 查询用户名是否被占用
     */
    @ResponseBody
    @PostMapping(value = "/account/ajax/check_username/")
    public AjaxResult checkUsername(@RequestParam(value = "username", required = false) String username)
    {
        if(StrUtils.isEmpty(username)){
            return AjaxResult.error(100,"用户名不能为空");
        }
        if(!StrUtils.checkUserName(username)){
            return AjaxResult.error(101,"用户名格式不正确");
        }
        User user = new User();
        user.setUsername(username);
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUsernameUnique(user)))
        {
            return AjaxResult.error(102,"用户名已存在");
        }
        return AjaxResult.success("用户可以注册");
    }

    /**
     * 查询邮箱是否被占用
     */
    @ResponseBody
    @PostMapping(value = "/account/ajax/check_email/")
    public AjaxResult checkEmail(@RequestParam(value = "useremail", required = false) String useremail)
    {
        if(StrUtils.isEmpty(useremail)){
            return AjaxResult.error(100,"邮箱不能为空");
        }
        if(!StrUtils.checkEmail(useremail)){
            return AjaxResult.error(101,"邮箱格式不正确");
        }
        User user = new User();
        user.setEmail(useremail);
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserEmailUnique(user)))
        {
            return AjaxResult.error(102,"邮箱已存在");
        }
        return AjaxResult.success("邮箱可以注册");
    }

    /**
     * 查询手机号是否被占用
     */
    @ResponseBody
    @PostMapping(value = "/account/ajax/check_mobile/")
    public AjaxResult checkMobile(@RequestParam(value = "mobile", required = false) String mobile)
    {
        if(StrUtils.isEmpty(mobile)){
            return AjaxResult.error(100,"手机号不能为空");
        }
        if(!StrUtils.checkPhoneNumber(mobile)){
            return AjaxResult.error(101,"手机号格式不正确");
        }
        User user = new User();
        user.setMobile(mobile);
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserEmailUnique(user)))
        {
            return AjaxResult.error(102,"手机号已存在");
        }
        return AjaxResult.success("手机号可以注册");
    }

    // 登出
    @GetMapping("/user/logout")
    public String logout(HttpSession session) {
        SessionUtils.removeUser();
        return "redirect:/index";
    }

    /**
     * 用户基本资料
     *
     * @return
     */
    @GetMapping(value = {"/user/setting/profile/" , "/user/setting/profile"})
    public String userSetting(ModelMap modelMap){
        User user = SessionUtils.getUser();
        UserInfoDTO info = userService.findUserInfoById(user.getId());
        modelMap.addAttribute("user", info);
        return theme.getPcTemplate("user/my_setting");
    }

    /**
     * 保存用户基本资料
     *
     * @return
     */
    @ResponseBody
    @PostMapping(value = "/user/setting/profile")
    public AjaxResult userSetting(User user){
        if(StrUtils.isEmpty(user.getNickname())){
            return AjaxResult.error("昵称不能为空");
        }
        user.setId(SessionUtils.getUser().getId());
        int conut=userService.updateUser(user);
        if(conut > 0){
            return AjaxResult.success("修改完成！");
        }
        return AjaxResult.error("未知错误！，请联系管理员！");
    }

    /**
     * 用户隐私设置
     *
     * @return
     */
    @GetMapping(value = {"/user/setting/privacy/" , "/user/setting/privacy"})
    public String userPrivacy(ModelMap modelMap){
        User user = SessionUtils.getUser();
        UserInfoDTO info = userService.findUserInfoById(user.getId());
        modelMap.addAttribute("user", info);
        return theme.getPcTemplate("user/my_privacy");
    }

    /**
     * 用户安全设置
     *
     * @return
     */
    @GetMapping(value = {"/user/setting/security/" , "/user/setting/security"})
    public String userSecurity(ModelMap modelMap){
        User user = SessionUtils.getUser();
        UserInfoDTO info = userService.findUserInfoById(user.getId());
        modelMap.addAttribute("user", info);
        return theme.getPcTemplate("user/my_security");
    }

    /**
     * 用户认证
     *
     * @return
     */
    @GetMapping(value = {"/user/setting/verify/" , "/user/setting/verify"})
    public String userPay(ModelMap modelMap){
        User user = SessionUtils.getUser();
        modelMap.addAttribute("user", user);
        return theme.getPcTemplate("user/my_verify");
    }

    /**
     * 用户私信列表
     *
     * @return
     */
    @GetMapping(value = {"/user/inbox/" , "/user/inbox"})
    public String userIndex(ModelMap modelMap){
        User user = SessionUtils.getUser();
        UserInfoDTO info = userService.findUserInfoById(user.getId());
        modelMap.addAttribute("user", info);
        return theme.getPcTemplate("user/my_inbox");
    }

}
