package com.flycms.modules.user.controller.front;

import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.domain.UserFans;
import com.flycms.modules.user.service.IUserFansService;
import com.flycms.modules.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
/**
 * 用户展示Controller
 *
 * @author kaifei sun
 * @date 2020-07-08
 */
@Controller
public class PeopleController extends BaseController
{
    @Resource
    private IUserService userService;

    @Autowired
    private IUserFansService userFansService;

    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    //登录处理
    @ResponseBody
    @PostMapping(value = "/user/follow")
    public AjaxResult userLogin(@RequestParam(value = "followId", required = false) String followId){
        if (!StrUtils.checkLong(followId)) {
            return AjaxResult.error(0,"id参数不正确");
        }
        User people = userService.findUserById(Long.valueOf(followId));
        if(people == null){
            return AjaxResult.error(1,"您关注的用户不存在");
        }
        User login = SessionUtils.getUser();
        if(login == null){
            return AjaxResult.error(2,"请登录后在关注操作");
        }
        if(!userFansService.checkUserFansUnique(Long.valueOf(followId),login.getId())){
            userFansService.deleteUserFansById(Long.valueOf(followId),login.getId());
            return AjaxResult.success(3,"已取消关注");
        }else{
            UserFans userFans=new UserFans();
            userFans.setFollowId(Long.valueOf(followId));
            userFans.setFansId(login.getId());
            userFansService.insertUserFans(userFans);
            return AjaxResult.success(4,"已成功关注");
        }
    }

    /**
     * 用户排名首页
     *
     * @return
     */
    @GetMapping(value = {"/people/" , "/people"})
    public String regUser(ModelMap modelMap){
        return theme.getPcTemplate("people/list_people");
    }


    /**
     * 用户首页
     *
     * @return
     */
    @GetMapping(value = "/people/{userId}")
    public String peoleIndex(@PathVariable("userId") String userId, ModelMap modelMap){
        if (!StrUtils.checkLong(userId)) {
            return forward("error/404");
        }
        User people = userService.findUserById(Long.valueOf(userId));
        if(people == null){
            return forward("error/404");
        }
        if(people.getStatus() == 0){
            modelMap.addAttribute("message","用户未审核或者被锁定，请联系管理员！");
            return theme.getPcTemplate("error/error_tip");
        }
        if(people.getDeleted() == 1){
            modelMap.addAttribute("message","用户已被删除或者不存在，请联系管理员！");
            return theme.getPcTemplate("error/error_tip");
        }
        User login = SessionUtils.getUser();
        modelMap.addAttribute("login",login);
        modelMap.addAttribute("people",people);
        return theme.getPcTemplate("people/index");
    }

    /**
     * 用户话题列表
     *
     * @return
     */
    @GetMapping(value = "/people/{userId}/topic/")
    public String regPhone(@PathVariable("userId") String userId, @PathVariable(value = "p", required = false) String p, ModelMap modelMap){
        if (!StrUtils.checkLong(userId)) {
            return forward("error/404");
        }
        User people = userService.findUserById(Long.valueOf(userId));
        if(people == null){
            return forward("error/404");
        }
        if(people.getStatus() == 0){
            modelMap.addAttribute("message","用户未审核或者被锁定，请联系管理员！");
            return theme.getPcTemplate("error/error_tip");
        }
        if(people.getDeleted() == 1){
            modelMap.addAttribute("message","用户已被删除或者不存在，请联系管理员！");
            return theme.getPcTemplate("error/error_tip");
        }
        User login = SessionUtils.getUser();
        if(p == null) p = "1";
        modelMap.addAttribute("p", p);
        modelMap.addAttribute("login",login);
        modelMap.addAttribute("people",people);
        return theme.getPcTemplate("people/list_topic");
    }

}
