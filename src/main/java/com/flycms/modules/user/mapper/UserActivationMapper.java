package com.flycms.modules.user.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.UserActivation;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 注册码Mapper接口
 * 
 * @author admin
 * @date 2020-12-08
 */
@Repository
public interface UserActivationMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增注册码
     *
     * @param userActivation 注册码
     * @return 结果
     */
    public int insertUserActivation(UserActivation userActivation);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除注册码
     *
     * @param id 注册码ID
     * @return 结果
     */
    public int deleteUserActivationById(Long id);

    /**
     * 批量删除注册码
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserActivationByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改注册码
     *
     * @param userActivation 注册码
     * @return 结果
     */
    public int updateUserActivation(UserActivation userActivation);

    /**
     * 按用户名（邮箱、手机号）+ 验证码查询修改验证状态为已验证，0未验证，1为已验证
     *
     * @param userName 用户名
     * @param code 验证码
     * @return 结果
     */
    public int updateUserActivationByStatus(String userName,String code);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询指定日期内申请验证码次数
     *
     * @param userName
     * @param referTime
     * @return
     */
    public int checkUserActivationCount(String userName,String referTime);

    /**
     * 查询验证码在当前时间加多少分钟内获取并且是否过时或不存在
     *
     * @param userActivation 注册码实体类
     * @return
     */
    public int checkUserActivationCode(UserActivation userActivation);



    /**
     * 查询注册码
     * 
     * @param id 注册码ID
     * @return 注册码
     */
    public UserActivation findUserActivationById(Long id);

    /**
     * 查询注册码数量
     *
     * @param pager 分页处理类
     * @return 注册码数量
     */
    public int queryUserActivationTotal(Pager pager);

    /**
     * 查询注册码列表
     * 
     * @param pager 分页处理类
     * @return 注册码集合
     */
    public List<UserActivation> selectUserActivationPager(Pager pager);

    /**
     * 查询需要导出的注册码列表
     *
     * @param userActivation 注册码
     * @return 注册码集合
     */
    public List<UserActivation> exportUserActivationList(UserActivation userActivation);
}
