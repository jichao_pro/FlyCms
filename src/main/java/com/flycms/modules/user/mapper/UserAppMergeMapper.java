package com.flycms.modules.user.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.user.domain.UserAppMerge;
import org.springframework.stereotype.Repository;

/**
 * 用户和app关联Mapper接口
 * 
 * @author admin
 * @date 2020-05-31
 */
@Repository
public interface UserAppMergeMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户和app关联
     *
     * @param userAppMerge 用户和app关联
     * @return 结果
     */
    public int insertUserAppMerge(UserAppMerge userAppMerge);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户和app关联
     *
     * @param appId 用户和app关联ID
     * @return 结果
     */
    public int deleteUserAppMergeById(Long appId);

    /**
     * 批量删除用户和app关联
     *
     * @param appIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserAppMergeByIds(Long[] appIds);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户和app关联
     *
     * @param userAppMerge 用户和app关联
     * @return 结果
     */
    public int updateUserAppMerge(UserAppMerge userAppMerge);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验APPID或者用户id是否唯一
     *
     * @param userAppMerge 用户和app关联ID
     * @return 结果
     */
    public int checkUserAppMergeUnique(UserAppMerge userAppMerge);


    /**
     * 查询用户和app关联
     * 
     * @param appId 用户和app关联ID
     * @return 用户和app关联
     */
    public UserAppMerge selectUserAppMergeById(Long appId);

    /**
     * 查询用户和app关联数量
     *
     * @param pager 分页处理类
     * @return 用户和app关联数量
     */
    public int queryUserAppMergeTotal(Pager pager);

    /**
     * 查询用户和app关联列表
     * 
     * @param pager 分页处理类
     * @return 用户和app关联集合
     */
    public List<UserAppMerge> selectUserAppMergePager(Pager pager);

}
