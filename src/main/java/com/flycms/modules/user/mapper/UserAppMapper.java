package com.flycms.modules.user.mapper;

import java.util.List;

import com.flycms.modules.user.domain.UserApp;
import com.flycms.common.utils.page.Pager;
import org.springframework.stereotype.Repository;

/**
 * 用户用小程序注册信息Mapper接口
 * 
 * @author admin
 * @date 2020-05-30
 */
@Repository
public interface UserAppMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户用小程序注册信息
     *
     * @param userApp 用户用小程序注册信息
     * @return 结果
     */
    public int insertUserApp(UserApp userApp);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除用户用小程序注册信息
     *
     * @param id 用户用小程序注册信息ID
     * @return 结果
     */
    public int deleteUserAppById(Long id);

    /**
     * 批量删除用户用小程序注册信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteUserAppByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户用小程序注册信息
     *
     * @param userApp 用户用小程序注册信息
     * @return 结果
     */
    public int updateUserApp(UserApp userApp);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验openid是否唯一
     *
     * @param userApp 用户用小程序注册信息ID
     * @return 结果
     */
    public int checkUserAppOpenIdUnique(UserApp userApp);

    /**
     * 校验unionid是否唯一
     *
     * @param userApp 用户用小程序注册信息ID
     * @return 结果
     */
    public int checkUserAppUnionIdUnique(UserApp userApp);


    /**
     * 查询用户用小程序注册信息
     * 
     * @param id 用户用小程序注册信息ID
     * @return 用户用小程序注册信息
     */
    public UserApp selectUserAppById(Long id);

    /**
     * 按openId查询用户用小程序注册信息详细信息
     *
     * @param openId 用户用小程序注册信息openId
     * @return 用户用小程序注册信息
     */
    public UserApp selectUserAppByOpenId(String openId);

    /**
     * 按UnionId查询用户用小程序注册信息详细信息
     *
     * @param unionId 用户用小程序注册信息unionId
     * @return 用户用小程序注册信息
     */
    public UserApp selectUserAppByUnionId(String unionId);

    /**
     * 查询用户用小程序注册信息数量
     *
     * @param pager 分页处理类
     * @return 用户用小程序注册信息数量
     */
    public int queryUserAppTotal(Pager pager);

    /**
     * 查询用户用小程序注册信息列表
     * 
     * @param pager 分页处理类
     * @return 用户用小程序注册信息集合
     */
    public List<UserApp> selectUserAppPager(Pager pager);

}
