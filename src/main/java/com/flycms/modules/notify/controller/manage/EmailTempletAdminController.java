package com.flycms.modules.notify.controller.manage;


import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.notify.domain.EmailTemplet;
import com.flycms.modules.notify.domain.dto.EmailTempletDTO;
import com.flycms.modules.notify.service.IEmailTempletService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 系统邮件模板设置Controller
 * 
 * @author admin
 * @date 2020-11-09
 */
@RestController
@RequestMapping("/system/notify/templet")
public class EmailTempletAdminController extends BaseController
{
    @Autowired
    private IEmailTempletService emailTempletService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增系统邮件模板设置
     */
    @PreAuthorize("@ss.hasPermi('notify:templet:add')")
    @Log(title = "系统邮件模板设置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody EmailTemplet emailTemplet)
    {
        return toAjax(emailTempletService.insertEmailTemplet(emailTemplet));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除系统邮件模板设置
     */
    @PreAuthorize("@ss.hasPermi('notify:templet:remove')")
    @Log(title = "系统邮件模板设置", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(emailTempletService.deleteEmailTempletByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改系统邮件模板设置
     */
    @PreAuthorize("@ss.hasPermi('notify:templet:edit')")
    @Log(title = "系统邮件模板设置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody EmailTemplet emailTemplet)
    {
        System.out.println("++++++++++++++++++"+emailTemplet.getContent());
        return toAjax(emailTempletService.updateEmailTemplet(emailTemplet));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询系统邮件模板设置列表
     */
    @PreAuthorize("@ss.hasPermi('notify:templet:list')")
    @GetMapping("/list")
    public TableDataInfo list(EmailTemplet emailTemplet,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<EmailTempletDTO> pager = emailTempletService.selectEmailTempletPager(emailTemplet, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出系统邮件模板设置列表
     */
    @PreAuthorize("@ss.hasPermi('notify:templet:export')")
    @Log(title = "系统邮件模板设置", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(EmailTemplet emailTemplet)
    {
        List<EmailTempletDTO> emailTempletList = emailTempletService.exportEmailTempletList(emailTemplet);
        ExcelUtil<EmailTempletDTO> util = new ExcelUtil<EmailTempletDTO>(EmailTempletDTO.class);
        return util.exportExcel(emailTempletList, "templet");
    }

    /**
     * 获取系统邮件模板设置详细信息
     */
    @PreAuthorize("@ss.hasPermi('notify:templet:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(emailTempletService.findEmailTempletById(id));
    }

}
