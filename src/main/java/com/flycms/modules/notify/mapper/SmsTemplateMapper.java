package com.flycms.modules.notify.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.SmsTemplate;
import org.springframework.stereotype.Repository;

/**
 * 短信模板Mapper接口
 * 
 * @author admin
 * @date 2020-05-27
 */
@Repository
public interface SmsTemplateMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信模板
     *
     * @param smsTemplate 短信模板
     * @return 结果
     */
    public int insertSmsTemplate(SmsTemplate smsTemplate);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除短信模板
     *
     * @param id 短信模板ID
     * @return 结果
     */
    public int deleteSmsTemplateById(Long id);

    /**
     * 批量删除短信模板
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSmsTemplateByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信模板
     *
     * @param smsTemplate 短信模板
     * @return 结果
     */
    public int updateSmsTemplate(SmsTemplate smsTemplate);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验签名id是否唯一
     *
     * @param smsTemplate 短信模板ID
     * @return 结果
     */
    public int checkSmsTemplateSignIdUnique(Long smsTemplate);

    /**
     * 校验模板名称是否唯一
     *
     * @param smsTemplate 短信模板ID
     * @return 结果
     */
    public int checkSmsTemplateTemplateNameUnique(String smsTemplate);


    /**
     * 查询短信模板
     * 
     * @param id 短信模板ID
     * @return 短信模板
     */
    public SmsTemplate selectSmsTemplateById(Long id);

    /**
     * 查询短信模板数量
     *
     * @param pager 分页处理类
     * @return 短信模板数量
     */
    public int querySmsTemplateTotal(Pager pager);

    /**
     * 查询短信模板列表
     * 
     * @param pager 分页处理类
     * @return 短信模板集合
     */
    public List<SmsTemplate> selectSmsTemplatePager(Pager pager);

}
