package com.flycms.modules.notify.mapper;

import java.util.List;

import com.flycms.modules.notify.domain.SmsCode;
import com.flycms.common.utils.page.Pager;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 短信验证码Mapper接口
 * 
 * @author kaifei sun
 * @date 2020-05-27
 */
@Repository
public interface SmsCodeMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增短信验证码
     *
     * @param smsCode 短信验证码
     * @return 结果
     */
    public int insertSmsCode(SmsCode smsCode);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除短信验证码
     *
     * @param id 短信验证码ID
     * @return 结果
     */
    public int deleteSmsCodeById(Long id);

    /**
     * 批量删除短信验证码
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSmsCodeByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改短信验证码
     *
     * @param smsCode 短信验证码
     * @return 结果
     */
    public int updateSmsCode(SmsCode smsCode);

    /**
     * 按用户名（邮箱、手机号）+ 验证码查询修改验证状态为已验证，0未验证，1为已验证
     *
     * @param userName  按用户名（邮箱、手机号）
     * @param code  验证码
     * @return
     */
    public int updateSmsCodeByStatus(@Param("userName") String userName,@Param("code") String code);
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询指定日期内申请验证码次数
     *
     * @param userId
     * @param userName
     * @param createTime
     * @return
     */
    public int findSmsCodeCount(@Param("userId") Long userId,@Param("userName") String userName,@Param("createTime") String createTime);

    /**
     * 查询验证码在当前时间5分钟内获取并且是否过时或不存在
     *
     * @param userId
     * @param userName
     * @param codeType 注册码类型：1手机注册验证码,2安全手机设置验证码,3密码重置验证码
     * @return
     */
    public SmsCode findSmsCodeByCode(@Param("userId") Long userId,@Param("userName") String userName,@Param("codeType") Integer codeType);

    /**
     * 查询短信验证码
     * 
     * @param id 短信验证码ID
     * @return 短信验证码
     */
    public SmsCode selectSmsCodeById(Long id);

    /**
     * 查询短信验证码数量
     *
     * @param pager 分页处理类
     * @return 短信验证码数量
     */
    public int querySmsCodeTotal(Pager pager);

    /**
     * 查询短信验证码列表
     * 
     * @param pager 分页处理类
     * @return 短信验证码集合
     */
    public List<SmsCode> selectSmsCodePager(Pager pager);

}
