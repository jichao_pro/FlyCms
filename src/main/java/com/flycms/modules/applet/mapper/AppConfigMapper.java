package com.flycms.modules.applet.mapper;

import java.util.List;

import com.flycms.modules.applet.domain.AppConfig;
import com.flycms.common.utils.page.Pager;
import org.springframework.stereotype.Repository;

/**
 * 小程序开发者ID设置Mapper接口
 * 
 * @author admin
 * @date 2020-05-27
 */
@Repository
public interface AppConfigMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增小程序开发者ID设置
     *
     * @param appConfig 小程序开发者ID设置
     * @return 结果
     */
    public int insertAppConfig(AppConfig appConfig);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小程序开发者ID设置
     *
     * @param id 小程序开发者ID设置ID
     * @return 结果
     */
    public int deleteAppConfigById(Long id);

    /**
     * 批量删除小程序开发者ID设置
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAppConfigByIds(Long[] ids);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小程序开发者ID设置
     *
     * @param appConfig 小程序开发者ID设置
     * @return 结果
     */
    public int updateAppConfig(AppConfig appConfig);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验平台名称和AppId是否唯一，更新时判断非当前id下是否时唯一
     *
     * @param appConfig 小程序开发者ID设置ID
     * @return 结果
     */
    public int checkAppConfigUnique(AppConfig appConfig);


    /**
     * 查询小程序开发者ID设置
     * 
     * @param id 小程序开发者ID设置ID
     * @return 小程序开发者ID设置
     */
    public AppConfig selectAppConfigById(Long id);

    /**
     * 查询小程序开发者ID设置数量
     *
     * @param pager 分页处理类
     * @return 小程序开发者ID设置数量
     */
    public int queryAppConfigTotal(Pager pager);

    /**
     * 查询小程序开发者ID设置列表
     * 
     * @param pager 分页处理类
     * @return 小程序开发者ID设置集合
     */
    public List<AppConfig> selectAppConfigPager(Pager pager);

}
