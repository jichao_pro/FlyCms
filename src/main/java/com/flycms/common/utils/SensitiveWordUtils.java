package com.flycms.common.utils;

import cn.hutool.dfa.WordTree;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * 敏感词处理工具 - DFA算法实现
 * <p>
 * 源码来自：https://www.jianshu.com/p/2e84eacc3cc8
 *
 * @author sam
 * @since 2017/9/4
 * <p>
 * 这个过滤算法简单是简单，但没法对多音字过滤，一个敏感词，如果用同音的字把其中一个字换了，就过滤不掉了，不过聊胜于无 :)
 */
public class SensitiveWordUtils {



    /**
     * 替换敏感字字符
     *
     * @param txt        文本
     * @param replaceStr 替换的字符串，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符串：[屏蔽]，替换结果：我爱[屏蔽]
     * @return
     */
    public static String replaceSensitiveWord(String txt, List<String> replaceStr) {
        if(!StringUtils.isBlank(txt)){
            for (final String str : replaceStr) {
                if(str.length() > 0){
                    txt = txt.replaceAll("["+str+"]", "*");
                }
            };
        }
        return txt.trim();
    }

        public static void main(String[] args) {

            WordTree tree = new WordTree();
            tree.addWord("大");
            tree.addWord("大土豆");
            tree.addWord("土豆");
            tree.addWord("刚出锅");
            tree.addWord("出锅");
            //正文
            String text = "sdfsaf我有一werwqrwq颗大&&土豆，刚出**锅的,很多大土豆，出锅了，刚】】】出****锅";
            List<String> matchAll = tree.matchAll(text, -1, true, true);
            System.out.println(replaceSensitiveWord(text,matchAll));
        }

}
