$(document).ready(function(){
    $('.dropdown-toggle').dropdown();
    $(':checkbox').checkboxpicker({
        offLabel:'影藏',
        onLabel:'显示'
    });
    //关注小组
    $(document).on('click', '#follow-button', function (){
        var groupId = $(this).attr("data-group-id");
        var text = $(this);
        $.ajax({
            url: '/group/user/follow',
            data: {"groupId":groupId},
            dataType: "json",
            type :  "post",
            cache : false,
            async: false,
            error : function(i, g, h) {
                layer.msg('发送错误', {icon: 2});
            },
            success: function(data){
                if(data.code==101){
                    layer.msg(data.msg, {icon: 2, time: 2000});
                    return false;
                }else if(data.code==501){
                    layer.msg(data.msg, {icon: 2, time: 2000});
                    return false;
                }else if(data.code==511){
                    text.removeClass('btn-success').addClass('alert-warning');
                    text.html("等待审核");
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 1, time: 2000});
                    return false;
                }else if(data.code==512){
                    text.removeClass('btn-success').addClass('alert-info');
                    text.html("加入小组");
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 1, time: 2000});
                    return false;
                }else if(data.code==520){
                    text.removeClass('btn-success').addClass('alert-info');
                    if(text.text() == "加入小组"){
                        text.html("退出小组");
                    }
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 1, time: 2000});
                    return false;
                }else if(data.code==521){
                    text.removeClass('alert-info').addClass('btn-success');
                    if(text.text() == "退出小组"){
                        text.html("加入小组");
                    }
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 2, time: 2000});
                    return false;
                }
            }
        });
    });

    $(document).on('click', '.add-educate', function (){
        var columnName=$(".classify").val(),sortOrder=$(".sort-control").val(),isshow=$("#isshow").val(),groupId=$("#groupId").val();
        $.ajax({
            url:'/user/group/addcolumn',
            type:'post',
            data:{'columnName':columnName,'sortOrder':sortOrder,"isshow":isshow,"groupId":groupId},
            dataType:'json',
            success:function(data){
                if(data.code==200){
                    layer.msg("发布成功", { shift: -1 }, function () {
                        //location.href = "/topic/"+data.data;
                    });
                }else{
                    alert(data.msg);
                }
            },
            error:function(){
                console.log('请求出错！');
            }
        })
    });
});